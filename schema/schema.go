package schema

import (
	"archive/tar"
)

type MultipleBlock struct {
	BlockSize       int64
	CompressOffsets []int64
	CompressSizes   []int64
}

type File struct {
	CompressOffset int64  // 文件起点
	CompressSize   int64  // 文件大小 压缩之后
	DataSha256     []byte // 数据文件的sha256
	FileIndex      int64  // 数据分片文件的索引
	Compress       int    // 0 没有压缩 1 snappy 整个文件 2 文件被snappy 分块压缩 3 文件存在于snappy解压之后数据的一部分
	TarHeader      *tar.Header
	MultipleBlock  *MultipleBlock
	Encrypt        *Encrypt
}

type Encrypt struct {
	Cipher int // 1:Salsa20
	Salt   []byte
}

type IndexMetadataFile struct {
	Files   []File
	Version string
}
