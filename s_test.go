package main

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	. "git.oschina.net/wkc/tar-fs/schema"
	pb "git.oschina.net/wkc/tar-fs/schemapb"
	"github.com/golang/snappy"
	"github.com/pkg/errors"
	ffjson "github.com/pquerna/ffjson/ffjson"
	"github.com/stretchr/testify/assert"
	"gopkg.in/vmihailenco/msgpack.v2"
	"os"
	"qiniupkg.com/x/log.v7"
	"testing"
)

func load() (*IndexMetadataFile, error) {
	f, err := os.Open("imgs4.tar.sfi")
	if err != nil {
		return nil, errors.Wrap(err, "open dstFilename")
	}
	defer f.Close()
	m := IndexMetadataFile{}
	err = gob.NewDecoder(snappy.NewReader(f)).Decode(&m)
	if err != nil {
		return nil, errors.Wrap(err, "write index file")
	}
	return &m, nil
}

var m *IndexMetadataFile

// func aa(a *[]byte) {
// 	*a = append(*a, 100)
// }

// func Test1(t *testing.T) {
// 	a := []byte{1, 2, 3}
// 	aa(&a)
// 	log.Println(a)
// }

func init() {
	// d, err := load()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// log.Info("file num", len(d.Files))
	// m = d
	// m.Files = m.Files[:1000]
}

func BenchmarkLoadMsgPack(b *testing.B) {
	buf, err := msgpack.Marshal(m)
	if err != nil {
		b.Fatal(err)
	}
	log.Info("msgpack size", len(buf), len(encodeSnappy(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var t IndexMetadataFile
		err = msgpack.Unmarshal(buf, &t)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkLoadJSON(b *testing.B) {
	buf, err := json.Marshal(m)
	if err != nil {
		b.Fatal(err)
	}
	log.Info("json size", len(buf), len(encodeSnappy(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var t IndexMetadataFile
		err = json.Unmarshal(buf, &t)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkLoadGob(b *testing.B) {
	var buf bytes.Buffer
	err := gob.NewEncoder(&buf).Encode(m)
	if err != nil {
		b.Fatal(err)
	}
	log.Info("gob size", len(buf.Bytes()), len(encodeSnappy(buf.Bytes())))
	s := buf.Bytes()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf := bytes.NewBuffer(s)
		var t IndexMetadataFile
		err = gob.NewDecoder(buf).Decode(&t)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkLoadFFJSON(b *testing.B) {
	buf, err := ffjson.Marshal(m)
	if err != nil {
		b.Fatal(err)
	}
	log.Info("ffjson size", len(buf), len(encodeSnappy(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var t IndexMetadataFile
		err = ffjson.Unmarshal(buf, &t)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkLoadPB(b *testing.B) {
	m2 := pb.IndexMetadataFile{}
	m2.Version = m.Version
	for _, v := range m.Files {
		h := v.TarHeader
		var mb *pb.MultipleBlock
		if v.MultipleBlock != nil {
			mb = &pb.MultipleBlock{
				BlockSize:       v.MultipleBlock.BlockSize,
				CompressOffsets: v.MultipleBlock.CompressOffsets,
				CompressSizes:   v.MultipleBlock.CompressSizes,
			}
		}
		f := pb.File{
			CompressOffset: v.CompressOffset,
			CompressSize:   v.CompressSize,
			DataSha256:     v.DataSha256,
			FileIndex:      v.FileIndex,
			Compress:       int32(v.Compress),
			MultipleBlock:  mb,
			Header: &pb.TarHeader{
				Name:       h.Name,
				Mode:       h.Mode,
				Uid:        int32(h.Uid),
				Gid:        int32(h.Gid),
				Size_:      h.Size,
				ModTime:    h.ModTime.UnixNano(),
				Typeflag:   int32(h.Typeflag),
				Linkname:   h.Linkname,
				Uname:      h.Uname,
				Gname:      h.Gname,
				Devmajor:   h.Devmajor,
				Devminor:   h.Devminor,
				AccessTime: h.AccessTime.UnixNano(),
				ChangeTime: h.ChangeTime.UnixNano(),
				Xattrs:     h.Xattrs,
			},
		}
		m2.Files = append(m2.Files, &f)
	}

	buf, err := m2.Marshal()
	if err != nil {
		b.Fatal(err)
	}
	log.Info("pb size", len(buf), len(encodeSnappy(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var t pb.IndexMetadataFile
		err = t.Unmarshal(buf)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func encodeSnappy(buf []byte) []byte {
	var r []byte
	return snappy.Encode(r, buf)
}

func TestEncrypt(t *testing.T) {
	assert := assert.New(t)
	e := NewEncryptSalsa20("hellofsds")
	originData := []byte("xxx")
	out := make([]byte, len(originData))
	salt := e.salt()
	e.encrypt(&out, originData, salt)
	// log.Info("salt", string(salt))
	r := make([]byte, len(originData))
	e.decrypt(&r, out, salt)
	// log.Info("out", string(out))
	// log.Info("r", string(r))
	assert.Equal(r, "xxx")
}

func TestNoopEncrypt(t *testing.T) {
	assert := assert.New(t)
	e := NewEncryptNoop("hellofsds")
	originData := []byte("xxx")
	out := make([]byte, len(originData))
	salt := e.salt()
	e.encrypt(&out, originData, salt)
	r := make([]byte, len(originData))
	e.decrypt(&r, out, salt)
	// log.Info("originData", string(originData))
	// log.Info("out", string(out))
	// log.Info("r", string(r))
	assert.Equal(r, "xxx")
}
